#!/bin/bash

echo "---------------------------------------------------------------------"
echo "------------------------------ MONGO --------------------------------"
echo "---------------------------------------------------------------------"

# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
# echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
# sudo apt-get update
# sudo apt-get install -y mongodb-org


sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org


echo "---------------------------------------------------------------------"
echo "---------------------------- √ DONE ---------------------------------"
echo "---------------------------------------------------------------------"