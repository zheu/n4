#!/bin/bash

echo "---------------------------------------------------------------------"
echo "-------------------------- NODE & NPM -------------------------------"
echo "---------------------------------------------------------------------"

#   Headache #2 – Installing Anything with NPM
echo "changing global node modules path"
mkdir /home/vagrant/.npm-g
echo prefix = /home/vagrant/.npm-g >> ~/.npmrc
echo "export PATH=/home/vagrant/.npm-g/bin:$PATH" >> /home/vagrant/.profile
source ~/.profile

# node & Co
echo "node & npm installing"
sudo curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
sudo apt-get install nodejs -y


echo "updating npm and setting a right path to npm"
npm install -g npm
sudo rm -rf /usr/bin/npm
sudo ln -s /home/vagrant/.npm-g/lib/node_modules/npm/bin/npm-cli.js /usr/bin/npm
echo "export NODE_PATH=/home/vagrant/.npm-g/lib/node_modules:$NODE_PATH" >> ~/.bashrc
source ~/.bashrc

echo "prefix = /home/vagrant/.npm-g" >> /home/vagrant/.npm-g/lib/node_modules/npm/npmrc

npm
echo "check if npm path is in /home/vagrant/.npm-g/lib/..."
echo $PATH


# curl https://www.npmjs.org/install.sh | sh

echo "---------------------------------------------------------------------"
echo "---------------------------- √ DONE ---------------------------------"
echo "---------------------------------------------------------------------"