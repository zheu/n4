#!/bin/bash

pref=$1
name=$2
code=$3
node=$4

repo="https://github.com/linnovate/mean.git"
# code="/home/vagrant/code"
# node="/home/vagrant/node"

mkdir -p "${code}/${pref}/"
cd "${code}/${pref}/"

if [ -d "$name" ] 
then 
	echo "pro directory exists"
else
	git clone "${repo}" "${name}"
	sudo mkdir -p "${node}/${pref}/${name}/node_modules"
	ln -s "${node}/${pref}/${name}/node_modules/" "${code}/${pref}/${name}/"
	cd "${code}/${pref}/${name}"
	npm cache clean
	sudo npm install
	sudo npm install swagger-node-express
	bower install
	echo "it seams all's DONE. Run 'npm install in the project folder'"
fi