class Proji
  def Proji.init(config, proji, scripts)  
    
    if (proji["projects-mode"] == 1)
      # config.vm.synced_folder proji["projects-on-host"], proji["projects-on-guest"] 
      if proji.has_key?("projects")
        proji["projects"].each do |project|
          if (project["mode"] == 1)
            config.vm.provision "shell" do |s|
              s.path = scripts + "/stacks/" + project["stck"] + ".stack.sh"
              s.args = [project["pref"], project["name"], proji["projects-on-guest"], proji["nodemodu-on-guest"]]
              s.privileged = project["priv"]
            end
          end
        end
      end
    end

  end  
end